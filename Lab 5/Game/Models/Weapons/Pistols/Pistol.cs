﻿using System;

namespace Models.Weapons.Pistols
{
    [Serializable]
    public abstract class Pistol : Weapon
    {
        public Pistol(string name, int damage, int fireRate, int bulletCount) : base(name, damage, fireRate, bulletCount)
        {
        }

        public abstract Pistol Clone();
    }
}

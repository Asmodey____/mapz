﻿using System;

namespace Models.Weapons.Rifles
{
    [Serializable]
    public abstract class Rifle : Weapon
    {
        public Rifle(string name, int damage, int fireRate, int bulletCount) : base(name, damage, fireRate, bulletCount)
        {
        }

        public abstract Rifle Clone();
    }
}

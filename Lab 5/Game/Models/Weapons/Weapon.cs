﻿using System;

namespace Models.Weapons
{
    [Serializable]
    public abstract class Weapon : Item
    {
        public int Damage { get; }

        public int FireRate { get; }

        public int BulletCount { get; }

        public static int MAX_BULLETS { get; protected set; }

        public Weapon(string name, int damage, int fireRate, int bulletCount) : base(name)
        {
            Damage = damage;
            FireRate = fireRate;
            BulletCount = bulletCount;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
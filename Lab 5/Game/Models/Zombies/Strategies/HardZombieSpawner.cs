﻿using System;

namespace Models.Zombies.Strategies
{
    public sealed class HardZombieSpawner : ZombieSpawner
    {
        public override void SpawnZombies(int wave)
        {
            for (int i = 0, count = (int)Math.Round(wave * 4.5); i < count; i++)
                _zombies.Add(CreateClassicZombie(wave));

            if (wave > 3)
            {
                for (int i = 0, count = (int)Math.Round(wave * 2.8); i < count; i++)
                    _zombies.Add(CreateTankZombie(wave));
            }

            if (wave > 5)
            {
                for (int i = 0, count = (int)Math.Round(wave * 2.2); i < count; i++)
                    _zombies.Add(CreateRunnerZombie(wave));
            }
        }
    }
}

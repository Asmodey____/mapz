﻿using Models.Zombies.Stats.Armors;
using Models.Zombies.Stats.Armors.ArmorDecorators;
using Models.Zombies.Stats.AttackTypes;
using Models.Zombies.Stats.Evasions;
using System;

namespace Models.Zombies.Factories
{
    //Temporary implementation
    public sealed class ClassicZombieStatsFactory : ZombieStatsFactory
    {
        public ClassicZombieStatsFactory()
        {
            _unchangebleFactor = 5;
        }

        public override Armor CreateArmor()
        {
            Random rand = new Random();
            var armor = new CommonArmor(_unchangebleFactor + rand.Next(_factor, 10 + _factor));

            if(_factor % 10 == 0)
            {
                CommonEvasion evasion = new CommonEvasion(15);
                EvasionArmor boostedArmor = new EvasionArmor(evasion, armor);
                return boostedArmor;
            }

            if(_factor % 5 == 0)
            {
                HpArmor boostedArmor = new HpArmor(_unchangebleFactor + 200 * _factor, armor);
                return boostedArmor;
            }

            return armor;
        }

        public override AttackType CreateAttackType()
        {
            return new CommonAttackType();
        }

        public override Evasion CreateEvasion()
        {
            Random rand = new Random();
            return new CommonEvasion(_unchangebleFactor + rand.Next(_factor, 10 + _factor));
        }

        public override int CreateKillReward()
        {
            return _unchangebleFactor + _factor * 10;
        }

        public override int CreateHp()
        {
            return 100;
        }

        public override string CreateName()
        {
            return "Classic Zombie";
        }
    }
}

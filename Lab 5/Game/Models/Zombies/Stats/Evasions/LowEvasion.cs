﻿namespace Models.Zombies.Stats.Evasions
{
    public sealed class LowEvasion : Evasion
    {
        public LowEvasion(int factor) : base(factor)
        {
        }

        public override string ToString()
        {
            return "Low Evasion";
        }
    }
}

﻿namespace Models.Zombies.Stats.Evasions
{
    public sealed class HighEvasion : Evasion
    {
        public HighEvasion(int factor) : base(factor)
        {
        }

        public override string ToString()
        {
            return "High Evasion";
        }
    }
}

﻿namespace Models.Zombies.Stats.Armors.ArmorDecorators
{
    public sealed class HpArmor : ArmorDecorator
    {
        public int GetAdditionalHp { get; }

        public HpArmor(int hp, Armor armor) : base(armor.Factor)
        {
            _armor = armor;
            GetAdditionalHp = hp;
        }

        public override string ToString()
        {
            return "Hp " + base.ToString();
        }
    }
}

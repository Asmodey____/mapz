﻿namespace Models.Zombies.Stats.Armors
{
    public sealed class CommonArmor : Armor
    {
        public CommonArmor(int factor) : base(factor)
        {
        }

        public override string ToString()
        {
            return "Common Armor";
        }
    }
}

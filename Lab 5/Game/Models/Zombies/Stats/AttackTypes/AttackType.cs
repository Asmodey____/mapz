﻿namespace Models.Zombies.Stats.AttackTypes
{
    public abstract class AttackType
    {
        public int Damage { get; protected set;
        }
        public abstract double Attack();
    }
}

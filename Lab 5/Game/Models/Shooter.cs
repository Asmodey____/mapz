﻿using Models.Weapons;
using System.Collections.Generic;
using System;
using System.Linq;
using Models.Zombies;
using Models.ShopClasses;
using System.Collections.ObjectModel;
using Models.Weapons.Pistols;
using Models.SavingClasses;

namespace Models
{
    public sealed class Shooter
    {
        public List<Weapon> AvailableWeapons { get; private set; } = new List<Weapon>();

        public List<Weapon> SelectedWeapon { get; } = new List<Weapon>();

        public Weapon CurrentWeapon { get; set; }

        public Shooter(Weapon startWeapon)
        {
            AvailableWeapons.Add(startWeapon);
            SelectedWeapon.Add(startWeapon);
            CurrentWeapon = startWeapon;
        }

        public void SetDefault()
        {
            AvailableWeapons.Add(new Glock());
            CurrentWeapon = AvailableWeapons[0];
        }

        public void SetState(AccountSaver saver)
        {
            AvailableWeapons = saver.AvailableWeapons;
            CurrentWeapon = saver.CurrentWeapon;
        }

        //returns remaining hp
        public int Shoot(Zombie zombie)
        {
            var result = zombie.GetDamage(CurrentWeapon.Damage);

            if (result == -1)
                return -1;
            else if (result > 0)
                return zombie.CurrentHp;
            else
                return 0;
        }

        public void SwitchWeapon(Weapon curWeapon, Weapon newWeapon)
        {
            var curWep = SelectedWeapon.FirstOrDefault(x => x == curWeapon);
            var newWep = AvailableWeapons.FirstOrDefault(x => x == newWeapon);

            if (curWep == null || newWep == null)
                throw new ArgumentException("This weapon is unavailable");

            if (curWep == newWep)
                throw new ArgumentException("Two weapons are the same");

            curWep = newWep;
        }
    }
}

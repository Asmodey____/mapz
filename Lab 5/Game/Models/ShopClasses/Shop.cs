﻿using Models.ShopClasses.ShopExceptions;
using Models.Weapons;
using Models.Weapons.Pistols;
using Models.Weapons.Rifles;
using Models.Weapons.SniperRifles;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Models.ShopClasses
{
    //singleton
    public sealed class Shop
    {
        private static readonly Lazy<Shop> _shop =
        new Lazy<Shop>(() => new Shop());

        public List<ShopItem> Weapons { get; }

        public List<ShopItem> Installations { get; }

        private Shop()
        {
            Weapons = new List<ShopItem>
        {
            new ShopItem(new Glock(), 0),
            new ShopItem(new Usp(), 300),
            new ShopItem(new Deagle(), 500),
            new ShopItem(new M16(), 2500),
            new ShopItem(new Ak47(), 3500),
            new ShopItem(new Scar(), 6000),
            new ShopItem(new Awm(), 9000),
            new ShopItem(new M24(), 12000),
            new ShopItem(new Mini14(), 15000)
        };

            Weapons[0].IsSoldOut = true;
            Installations = null; //temp
        }

        public int BuyWeapon(ShopItem item, int money, List<Weapon> availableWeapon)
        {
            if (_shop.Value.Weapons.IndexOf(item) == -1)
                throw new ArgumentException("Something went wrong!");

            if (item.IsSoldOut)
                throw new ItemSoldOutException("Item is already bought!");

            if (item.Price > money)
                throw new NotEnoughMoneyException("Not enough money!");

            int rest = money - item.Price;
            item.IsSoldOut = true;
            var weapon = item.GetItem as Weapon;
            availableWeapon.Add(weapon);

            return rest;
        }

        public static Shop GetShopInstance()
        {
            return _shop.Value;
        }
    }
}

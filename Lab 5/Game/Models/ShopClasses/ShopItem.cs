﻿namespace Models.ShopClasses
{
    public sealed class ShopItem
    {
        public Item GetItem { get; }

        public int Price { get; }

        public bool IsSoldOut { get; set; } = false;

        public ShopItem(Item item, int price)
        {
            GetItem = item;
            Price = price;
        }
    }
}

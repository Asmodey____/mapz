﻿using Game.ShopView;
using Game.ViewModels;
using Models.ShopClasses;
using Models.Weapons;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Game
{
    /// <summary>
    /// Логика взаимодействия для ShopWindow.xaml
    /// </summary>
    public partial class ShopWindow : Window
    {
        private readonly List<string> IMAGES_PATHS = new List<string>()
        {
            "/images/Glock.png",
            "/images/Usp.png",
            "/images/Deagle.png",
            "/images/M16.png",
            "/images/Ak47.png",
            "/images/Scar.png",
            "/images/Awm.png",
            "/images/M24.png",
            "/images/Mini14.png"
        };

        private List<ShopViewItem> _items = new List<ShopViewItem>();

        public ShopWindow(ref int money, List<Weapon> availableWeapons)
        {
            InitializeComponent();

            _items.Add(new ShopViewItem(
                new VisualShopItem("/images/weapons/Glock.png", Shop.GetShopInstance().Weapons[0]),
                new VisualShopItem("/images/weapons/Usp.png", Shop.GetShopInstance().Weapons[1]),
                new VisualShopItem("/images/weapons/Deagle.png", Shop.GetShopInstance().Weapons[2])));

            _items.Add(new ShopViewItem(
                new VisualShopItem("/images/weapons/M16.png", Shop.GetShopInstance().Weapons[3]),
                new VisualShopItem("/images/weapons/Ak47.png", Shop.GetShopInstance().Weapons[4]),
                new VisualShopItem("/images/weapons/Scar.png", Shop.GetShopInstance().Weapons[5])));

            _items.Add(new ShopViewItem(
                new VisualShopItem("/images/weapons/Awm.png", Shop.GetShopInstance().Weapons[6]),
                new VisualShopItem("/images/weapons/M24.png", Shop.GetShopInstance().Weapons[7]),
                new VisualShopItem("/images/weapons/Mini14.png", Shop.GetShopInstance().Weapons[8])));

            GlockListView.ItemsSource = _items;

            this.DataContext = new ShopViewModel(Shop.GetShopInstance(), ref money, availableWeapons);
        }
    }
}
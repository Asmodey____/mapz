﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab_1
{
    public sealed class Tree
    {
        public TreeNode Parent { get; private set; }

        public TreeNode ParentOpt { get; private set; }

        private int _currentPos;

        private Stack<TreeNode> _blocks = new Stack<TreeNode>(); 

        public Tree(List<Token> tokens)
        {
            Parent = BuildTree(tokens);
            _blocks.Clear();
            ParentOpt = BuildTree(tokens);
            Optimize(ParentOpt);
        }

        public TreeNode BuildTree(List<Token> tokens)
        {
            TreeNode parent = new TreeNode(null, null, new List<TreeNode>());
            _blocks.Push(parent);

            int count = tokens.Count();
            TreeNode currentNode = parent;

            for (_currentPos = 0; _currentPos < count; _currentPos++)
            {
                Token prevToken = new Token();
                Token nextToken = new Token();

                if (_currentPos - 1 >= 0)
                    prevToken = tokens[_currentPos - 1];
                if (_currentPos + 1 < count)
                    nextToken = tokens[_currentPos + 1];

                TreeNode left = null;
                TreeNode right = null;


                switch(tokens[_currentPos].Type)
                {
                    case TokenType.IF:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);

                        _blocks.Push(currentNode);
                        break;
                    case TokenType.ELSE_IF:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);

                        _blocks.Push(currentNode);
                        break;
                    case TokenType.ELSE:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);

                        _blocks.Push(currentNode);
                        break;
                    case TokenType.WHILE:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);

                        _blocks.Push(currentNode);
                        break;
                    case TokenType.RIGHT_BRACE:
                        _blocks.Pop();

                        break;
                    case TokenType.ASSIGN_OP:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        left = new TreeNode(prevToken, currentNode, null);
                        right = new TreeNode(nextToken, currentNode, null);
                        currentNode.Children.Add(left);
                        currentNode.Children.Add(right);

                        break;

                    #region comparison
                    case TokenType.GREATER:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        left = new TreeNode(prevToken, currentNode, null);
                        right = new TreeNode(nextToken, currentNode, null);
                        currentNode.Children.Add(left);
                        currentNode.Children.Add(right);

                        break;
                    case TokenType.GREATER_OR_EQUAL:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        left = new TreeNode(prevToken, currentNode, null);
                        right = new TreeNode(nextToken, currentNode, null);
                        currentNode.Children.Add(left);
                        currentNode.Children.Add(right);

                        break;
                    case TokenType.EQUALS:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        left = new TreeNode(prevToken, currentNode, null);
                        right = new TreeNode(nextToken, currentNode, null);
                        currentNode.Children.Add(left);
                        currentNode.Children.Add(right);

                        break;
                    case TokenType.LESS:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        left = new TreeNode(prevToken, currentNode, null);
                        right = new TreeNode(nextToken, currentNode, null);
                        currentNode.Children.Add(left);
                        currentNode.Children.Add(right);

                        break;
                    case TokenType.LESS_OR_EQUAL:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        left = new TreeNode(prevToken, currentNode, null);
                        right = new TreeNode(nextToken, currentNode, null);
                        currentNode.Children.Add(left);
                        currentNode.Children.Add(right);

                        break;
                    case TokenType.NOT_EQUALS:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        left = new TreeNode(prevToken, currentNode, null);
                        right = new TreeNode(nextToken, currentNode, null);
                        currentNode.Children.Add(left);
                        currentNode.Children.Add(right);

                        break;
                    #endregion

                    case TokenType.SUBMIT:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        for(int i = _currentPos; ; i++)
                        {
                            if(tokens[i].Type == TokenType.STRING_LIT)
                            {
                                left = new TreeNode(tokens[i], currentNode, null);
                                break;
                            }
                        }
                        
                        right = null;
                        currentNode.Children.Add(left);

                        break;
                    case TokenType.CLICK:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        for (int i = _currentPos; ; i++)
                        {
                            if (tokens[i].Type == TokenType.STRING_LIT)
                            {
                                left = new TreeNode(tokens[i], currentNode, null);
                                break;
                            }
                        }

                        right = null;
                        currentNode.Children.Add(left);

                        break;
                    case TokenType.OPEN:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        for (int i = _currentPos; ; i++)
                        {
                            if (tokens[i].Type == TokenType.STRING_LIT)
                            {
                                left = new TreeNode(tokens[i], currentNode, null);
                                break;
                            }
                        }

                        right = null;
                        currentNode.Children.Add(left);

                        break;
                    case TokenType.PRINT_TEXT:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        for (int i = _currentPos; ; i++)
                        {
                            if (tokens[i].Type == TokenType.STRING_LIT || tokens[i].Type == TokenType.IDENT)
                            {
                                left = new TreeNode(tokens[i], currentNode, null);
                                break;
                            }
                        }

                        right = null;
                        currentNode.Children.Add(left);

                        break;
                    case TokenType.FILL_TEXT:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);
                        bool isLeftSetted = false;

                        for (int i = _currentPos; ; i++)
                        {
                            if (!isLeftSetted && tokens[i].Type == TokenType.STRING_LIT)
                            {
                                left = new TreeNode(tokens[i], currentNode, null);
                                isLeftSetted = true;
                            }
                            else if(isLeftSetted && tokens[i].Type == TokenType.STRING_LIT)
                            {
                                right = new TreeNode(tokens[i], currentNode, null); ;
                                break;
                            }
                        }

                        currentNode.Children.Add(left);
                        currentNode.Children.Add(right);

                        break;
                    case TokenType.BACK:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), null);
                        _blocks.Peek().Children.Add(currentNode);
                        break;
                    case TokenType.FORWARD:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), null);
                        _blocks.Peek().Children.Add(currentNode);
                        break;
                    case TokenType.REFRESH:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), null);
                        _blocks.Peek().Children.Add(currentNode);
                        break;
                    case TokenType.INCREMENT:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);

                        if (tokens[_currentPos-1].Type == TokenType.IDENT)
                        {
                            left = new TreeNode(tokens[_currentPos-1], currentNode, null);
                        }
                        else
                        {
                            throw new Exception("Bad operand!");
                        }

                        right = null;
                        currentNode.Children.Add(left);

                        break;
                    case TokenType.DECREMENT:
                        currentNode = new TreeNode(tokens[_currentPos], _blocks.Peek(), new List<TreeNode>());
                        _blocks.Peek().Children.Add(currentNode);

                        if (tokens[_currentPos - 1].Type == TokenType.IDENT)
                        {
                            left = new TreeNode(tokens[_currentPos - 1], currentNode, null);
                        }
                        else
                        {
                            throw new Exception("Bad operand!");
                        }

                        right = null;
                        currentNode.Children.Add(left);

                        break;
                }
            }

            return parent;
        }

        private TreeNode BuildNode(Token value, TreeNode parent, List<TreeNode> childs)
        {
            return new TreeNode(value, parent, childs);
        }

        private TreeNode Optimize(TreeNode currentNode)
        {
            bool isIfPassed = false;

            if (currentNode.Children == null)
                return currentNode;

            for(int i = 0; i < currentNode.Children.Count(); i++)
            {
                if(currentNode.Value != null && 
                    (currentNode.Value.Type == TokenType.IF || 
                    currentNode.Value.Type == TokenType.ELSE_IF ||
                    currentNode.Value.Type == TokenType.WHILE))
                {
                    bool expressionRes = false;

                    var expressionNode = currentNode.Children[0];

                    if (expressionNode.Children[0].Value.Type == TokenType.NUMBER_LIT &&
                        expressionNode.Children[1].Value.Type == TokenType.NUMBER_LIT)
                    {
                        switch (expressionNode.Value.Type)
                        {
                            case TokenType.EQUALS:
                                expressionRes = Double.Parse(expressionNode.Children[0].Value.Data) ==
                                    Double.Parse(expressionNode.Children[1].Value.Data);
                                break;
                            case TokenType.NOT_EQUALS:
                                expressionRes = Double.Parse(expressionNode.Children[0].Value.Data) !=
                                    Double.Parse(expressionNode.Children[1].Value.Data);
                                break;
                            case TokenType.LESS:
                                expressionRes = Double.Parse(expressionNode.Children[0].Value.Data) <
                                    Double.Parse(expressionNode.Children[1].Value.Data);
                                break;
                            case TokenType.LESS_OR_EQUAL:
                                expressionRes = Double.Parse(expressionNode.Children[0].Value.Data) <=
                                    Double.Parse(expressionNode.Children[1].Value.Data);
                                break;
                            case TokenType.GREATER:
                                expressionRes = Double.Parse(expressionNode.Children[0].Value.Data) >
                                    Double.Parse(expressionNode.Children[1].Value.Data);
                                break;
                            case TokenType.GREATER_OR_EQUAL:
                                expressionRes = Double.Parse(expressionNode.Children[0].Value.Data) >=
                                    Double.Parse(expressionNode.Children[1].Value.Data);
                                break;
                        }

                        if (!expressionRes)
                        {
                            var nodeTemp = currentNode;
                            currentNode = currentNode.Parent;
                            currentNode.Children.Remove(nodeTemp);
                        }
                        else if(expressionRes && isIfPassed == false)
                        {
                            var nodeTemp = currentNode;
                            currentNode = currentNode.Parent;

                            for(int j = 0; j < currentNode.Children.Count(); )
                            {
                                if (currentNode.Children[j].Value.Type == TokenType.ELSE && currentNode.Children[j] != nodeTemp)
                                {
                                    currentNode.Children.RemoveAt(j);
                                    break;
                                }
                                else if((currentNode.Children[j].Value.Type == TokenType.ELSE_IF ||
                                    currentNode.Children[j].Value.Type == TokenType.IF) && 
                                    currentNode.Children[j] != nodeTemp)
                                {
                                    currentNode.Children.RemoveAt(j);
                                }
                                else
                                {
                                    j++;
                                }
                            }

                            nodeTemp.Children.RemoveAt(0);
                            currentNode.Children.InsertRange(currentNode.Children.IndexOf(nodeTemp), nodeTemp.Children);

                            for(int j = 0; j < nodeTemp.Children.Count(); j++)
                            {
                                nodeTemp.Children[j].Parent = nodeTemp.Parent;
                            }

                            currentNode.Children.Remove(nodeTemp);
                        }
                    }
                    else if(currentNode.Value.Type == TokenType.IF ||
                        currentNode.Value.Type == TokenType.ELSE_IF)
                    {
                        isIfPassed = true;
                    }
                    else if(currentNode.Value.Type == TokenType.ELSE)
                    {
                        isIfPassed = false;
                    }
                }
                else if (currentNode.Value != null && 
                    currentNode.Value.Type == TokenType.ELSE && 
                    currentNode.Parent.Children.Count() > 1)
                {
                    int isElseIfOrIf = currentNode.Parent.Children.FindIndex(x => x.Value.Type == TokenType.ELSE_IF ||
                                                                                        x.Value.Type == TokenType.IF);

                    if (isElseIfOrIf == -1)
                    {
                        currentNode.Parent.Children.InsertRange(currentNode.Parent.Children.IndexOf(currentNode),currentNode.Children);

                        for(int j = 0; j < currentNode.Children.Count(); j++)
                        {
                            currentNode.Children[j].Parent = currentNode.Parent;
                        }

                        var tempNode = currentNode;
                        currentNode = currentNode.Parent;
                        currentNode.Children.Remove(tempNode);
                    }
                }

                Optimize(currentNode.Children[i]);
            }

            return currentNode;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using OpenQA.Selenium.Chrome;

namespace Lab_1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private ChromeDriver driver;

        private Parser _parser;

        private Tree tree;

        private Stack<TreeViewItem> _blocks = new Stack<TreeViewItem>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TextBlockResult.Text = "";

            treeViewOpt.Items.Clear();
            treeView.Items.Clear();

            if(_parser != null && _parser.Driver != null)
                _parser.Driver.Quit();

            try
            {
                _parser = new Parser(TextBoxSourceCode.Text);
                tree = _parser.GetTree();
                RenderTree(tree.Parent, treeView);
                _blocks.Clear();
                RenderTree(tree.ParentOpt, treeViewOpt);

                foreach (var item in _parser.PullToPrint)
                {
                    TextBlockResult.Text += item;
                }
            }
            catch(Exception ex)
            {
                TextBlockResult.Text = ex.Message;
            }
        }

        private TreeNode RenderTree(TreeNode lastNode, TreeView view)
        {
            TreeNode currentNode = lastNode;
        
            if(currentNode.Children != null && currentNode.Children.Count() != 0)
            {
                TreeViewItem node = new TreeViewItem() { Header = currentNode.Value, IsExpanded = true };

                if (_blocks.Count() != 0)
                    _blocks.Peek().Items.Add(node);
                else
                {
                    view.Items.Add(node);
                }
                    
                _blocks.Push(node);

            }
            else if(currentNode.Value != null && _blocks.Count() != 0)
            {
                _blocks.Peek().Items.Add(new TreeViewItem() { Header = currentNode.Value, IsExpanded = true });
            }
            else
            {
                view.Items.Add(new TreeViewItem() { Header = currentNode.Value, IsExpanded = true });
            }

            while(currentNode.Children != null && currentNode.Children.Count() != 0)
            {
                RenderTree(currentNode.Children[0], view);
                currentNode.Children.RemoveAt(0);

                if (currentNode.Children.Count() == 0)
                {
                    _blocks.Pop();
                }
            }

            return currentNode;
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void ScrollViewer_PreviewMouseWheel_1(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void ScrollViewer_PreviewMouseWheel_2(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(_parser != null && _parser.Driver != null)
                _parser.Driver.Quit();
        }
    }
}

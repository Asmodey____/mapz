﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1
{
    public sealed class Variable
    {
        public string Name { get; private set; }

        public string Type { get; set; } // number/string

        public string Value { get; set; }

        public Variable(string name, string type = "undefined" ,string value = "undefined")
        {
            Name = name;
            Value = value;
            Type = type;
        }
    }
}

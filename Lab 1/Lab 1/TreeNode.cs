﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1
{
    public sealed class TreeNode
    {
        public Token Value { get; set; }

        public TreeNode Parent { get; set; }

        public List<TreeNode> Children { get; set; }

        public TreeNode(Token value, TreeNode parent, List<TreeNode> children)
        {
            Value = value;
            Parent = parent;
            Children = children;
        }

        public override string ToString()
        {
            return "Value: " + Value + " Parent: " + Parent;
        }
    }
}

﻿using System;

namespace Lab_1
{
    public sealed class Statement
    {
        public int BodyIndex { get; set; }

        public bool Result { get; set; }

        public TokenType Block { get; set; }

        public Statement(int bodyIndex, TokenType block, bool result)
        {
            if (block != TokenType.IF && block != TokenType.WHILE)
                throw new ArgumentException("Bad arguments!");

            BodyIndex = bodyIndex;
            Block = block;
            Result = result;
        }
    }
}

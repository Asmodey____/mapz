﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Chrome;

namespace Lab_1
{
    public sealed class Parser
    {
        private int _currentPos = 0;

        private List<Token> _tokens;

        private List<Token> _arguments = new List<Token>();

        private List<Variable> _variables = new List<Variable>();

        private Tree _tree;

        public Tree GetTree()
        {
            return _tree;
        }

        public ChromeDriver Driver { get; private set; }
        

        private Lexer _lexer;

        public Lexer GetLexer { get { return _lexer; } }

        public List<string> PullToPrint { get; private set; }

        private Stack<Statement> _blocks = new Stack<Statement>();

        public Parser(string str)
        {
            _lexer = new Lexer(str);
            _tokens = new List<Token>();
            PullToPrint = new List<string>();

            while (true)
            {
                Token token = _lexer.GetToken();

                if (token.Type == TokenType.EOF)
                {
                    _tokens.Add(token);
                    break;
                }
                else if (token.Type != TokenType.WHITE_SPACE && token.Type != TokenType.NEW_LINE && token.Type != TokenType.COMMENT && token.Type != TokenType.UNDEFINED)
                {
                    _tokens.Add(token);
                }
            }

            _tree = new Tree(_tokens);
            Parse(_tokens);
        }

        //opens url using web driver
        private void OpenUrl(string url)
        {
            if(Driver == null)
                Driver = new ChromeDriver();

            Driver.Navigate().GoToUrl(url);
        }

        public void Parse(List<Token> tokens)
        {
            int leftBraceIndex = -1;
            int rightBraceIndex = -1;

            double left;
            double right;

            Variable v1 = null;
            Variable v2 = null;

            bool statementResult = false;

            
            bool isIfStatement = false;
            bool isElseStatement = false;

            TokenType lastStatement = TokenType.EOF;

            bool isOpenFunction = false;
            bool isSubmitFunction = false;
            bool isFillTextFuction = false;
            bool isPrintFunction = false;
            bool isClickFunction = false;
            bool isRefreshFunction = false;
            bool isForwardFunction = false;
            bool isBackFunction = false;
            bool isDoubleClickFunction = false;

            bool isFunction = false;

            int bodyIndex = 0;

            int count = tokens.Count();

            for (_currentPos = 0; _currentPos < count;_currentPos++)
            {
                Token prevToken = null;
                Token nextToken = null;

                if (_currentPos - 1 >= 0)
                    prevToken = _tokens[_currentPos - 1];

                if (_currentPos + 1 < count)
                    nextToken = _tokens[_currentPos + 1];

                switch (_tokens[_currentPos].Type)
                {
                    case TokenType.NUMBER_LIT:
                        break;
                    case TokenType.VAR_KEYWORD:
                        if (nextToken.Type == TokenType.IDENT)
                            _variables.Add(new Variable(nextToken.Data));
                        else
                            throw new Exception("Bad syntax!");

                        break;
                    case TokenType.IDENT:
                        if (_variables.FindIndex(x => x.Name == tokens[_currentPos].Data) == -1)
                            throw new Exception("Bad symbols!");

                        break;
                    case TokenType.ASSIGN_OP:
                        if (prevToken == null || nextToken == null)
                            throw new Exception("Bad syntax!");

                        if (_variables.FirstOrDefault(x => x.Name == prevToken.Data) != null)
                        {
                            double doubleValue;
                            if (Double.TryParse(nextToken.Data.Replace('.', ','), out doubleValue))
                            {
                                _variables.FirstOrDefault(x => x.Name == prevToken.Data).Type = "number";
                            }

                            _variables.FirstOrDefault(x => x.Name == prevToken.Data).Value = nextToken.Data;
                        }
                        break;
                    case TokenType.EQUALS:
                        if (prevToken == null || nextToken == null)
                            throw new Exception("Bad syntax!");

                        double leftParam;
                        double rightParam;

                        if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.EQUALS);
                        }
                        else if(prevToken.Type == TokenType.NUMBER_LIT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = null;
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.EQUALS);
                        }
                        else if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.NUMBER_LIT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = null;

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.EQUALS);
                        }
                        else
                        {
                            v1 = null;
                            v2 = null;

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.EQUALS);
                        }

                        break;
                    case TokenType.NOT_EQUALS:
                        if (prevToken == null || nextToken == null)
                            throw new Exception("Bad syntax!");

                        if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.NOT_EQUALS);
                        }
                        else if (prevToken.Type == TokenType.NUMBER_LIT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = null;
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.NOT_EQUALS);
                        }
                        else if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.NUMBER_LIT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = null;

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.NOT_EQUALS);
                        }
                        else
                        {
                            v1 = null;
                            v2 = null;

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.NOT_EQUALS);
                        }

                        break;
                    case TokenType.LESS_OR_EQUAL:
                        if (prevToken == null || nextToken == null)
                            throw new Exception("Bad syntax!");

                        if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.LESS_OR_EQUAL);
                        }
                        else if (prevToken.Type == TokenType.NUMBER_LIT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = null;
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.LESS_OR_EQUAL);
                        }
                        else if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.NUMBER_LIT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = null;

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.LESS_OR_EQUAL);
                        }
                        else
                        {
                            v1 = null;
                            v2 = null;

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.LESS_OR_EQUAL);
                        }

                        break;
                    case TokenType.LESS:
                        if (prevToken == null || nextToken == null)
                            throw new Exception("Bad syntax!");

                        if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.LESS);
                        }
                        else if (prevToken.Type == TokenType.NUMBER_LIT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = null;
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.LESS);
                        }
                        else if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.NUMBER_LIT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = null;

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.LESS);
                        }
                        else
                        {
                            v1 = null;
                            v2 = null;

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.LESS);
                        }

                        break;
                    case TokenType.GREATER_OR_EQUAL:
                        if (prevToken == null || nextToken == null)
                            throw new Exception("Bad syntax!");

                        if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.GREATER_OR_EQUAL);
                        }
                        else if (prevToken.Type == TokenType.NUMBER_LIT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = null;
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.GREATER_OR_EQUAL);
                        }
                        else if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.NUMBER_LIT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = null;

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.GREATER_OR_EQUAL);
                        }
                        else
                        {
                            v1 = null;
                            v2 = null;

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.GREATER_OR_EQUAL);
                        }

                        break;
                    case TokenType.GREATER:
                        if (prevToken == null || nextToken == null)
                            throw new Exception("Bad syntax!");

                        if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.GREATER);
                        }
                        else if (prevToken.Type == TokenType.NUMBER_LIT && nextToken.Type == TokenType.IDENT)
                        {
                            v1 = null;
                            v2 = _variables.FirstOrDefault(x => x.Name == nextToken.Data);

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(v2.Value.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.GREATER);
                        }
                        else if (prevToken.Type == TokenType.IDENT && nextToken.Type == TokenType.NUMBER_LIT)
                        {
                            v1 = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            v2 = null;

                            if (!Double.TryParse(v1.Value.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.GREATER);
                        }
                        else
                        {
                            v1 = null;
                            v2 = null;

                            if (!Double.TryParse(prevToken.Data.Replace('.', ','), out leftParam) || !Double.TryParse(nextToken.Data.Replace('.', ','), out rightParam))
                                throw new ArgumentException("Bad arguments!");
                            else
                                statementResult = Comparise(leftParam, rightParam, TokenType.GREATER);
                        }

                        break;
                    case TokenType.IF:
                        statementResult = false;
                        isIfStatement = true;

                        if (nextToken.Type != TokenType.LEFT_BRACKET)
                            throw new Exception("Bad syntax!");

                        _blocks.Push(new Statement(_currentPos + 1, TokenType.IF, false));
                        break;
                    case TokenType.ELSE_IF:
                        if (prevToken.Type != TokenType.RIGHT_BRACE)
                            throw new Exception("Bad syntax!");

                        break;
                    case TokenType.ELSE:
                        if (prevToken.Type != TokenType.RIGHT_BRACE)
                            throw new Exception("Bad syntax!");

                        isElseStatement = true;

                        break;
                    case TokenType.WHILE:

                        if (nextToken.Type != TokenType.LEFT_BRACKET)
                            throw new Exception("Bad syntax!");

                        _blocks.Push(new Statement(_currentPos + 1, TokenType.WHILE, false));
                        break;
                    case TokenType.LEFT_BRACKET:
                        if (isFunction)
                        {
                            _arguments.Add(nextToken);
                        }
                        break;
                    case TokenType.COMMA:
                        if (isFunction)
                        {
                            _arguments.Add(nextToken);
                        }
                        break;
                    case TokenType.RIGHT_BRACKET:
                        if (isOpenFunction)
                        {
                            if (Driver == null)
                                Driver = new ChromeDriver();

                            string url = _arguments[0].Data.Substring(1, _arguments[0].Data.Length - 2);
                            OpenUrl(url);
                            isOpenFunction = false;
                            isFunction = false;
                        }
                        else if (isSubmitFunction)
                        {
                            if (Driver == null)
                                Driver = new ChromeDriver();

                            Submit(_arguments[0].Data.Substring(1, _arguments[0].Data.Length - 2));
                            isSubmitFunction = false;
                            isFunction = false;
                        }
                        else if (isFillTextFuction)
                        {
                            if (Driver == null)
                                Driver = new ChromeDriver();

                            string id = _arguments[0].Data.Substring(1, _arguments[0].Data.Length - 2);
                            string text = _arguments[1].Data.Substring(1, _arguments[1].Data.Length - 2);

                            FillText(id, text);
                            isFillTextFuction = false;
                            isFunction = false;
                        }
                        else if(isPrintFunction)
                        {
                            if (_variables.FindIndex(x => x.Name == _arguments[0].Data) != -1)
                            {
                                PullToPrint.Add(_variables.FirstOrDefault(x => x.Name == _arguments[0].Data).Value);
                                PullToPrint.Add("\n");
                            }
                            else if(_arguments[0].Type == TokenType.STRING_LIT)
                            {
                                PullToPrint.Add(_arguments[0].Data.Substring(1, _arguments[0].Data.Length - 2));
                                PullToPrint.Add("\n");
                            }
                            else
                            {
                                throw new ArgumentException("Bad arguments!");
                            }

                            isPrintFunction = false;
                            isFunction = false;
                        }
                        else if(isClickFunction)
                        {
                            string id = _arguments[0].Data.Substring(1, _arguments[0].Data.Length - 2);
                            var element = Driver.FindElementById(id);
                            element.Click();
                            isClickFunction = false;
                            isFunction = false;
                        }
                        else if(isBackFunction)
                        {
                            Driver.Navigate().Back();
                            isBackFunction = false;
                            isFunction = false;
                        }
                        else if(isRefreshFunction)
                        {
                            Driver.Navigate().Refresh();
                            isRefreshFunction = false;
                            isFunction = false;
                        }
                        else if(isForwardFunction)
                        {
                            Driver.Navigate().Forward();
                            isForwardFunction = false;
                            isFunction = false;
                        }
                        

                        break;
                    case TokenType.LEFT_BRACE:

                        if (_blocks.Count() != 0)
                        {
                            if(!statementResult && _blocks.Peek().Block == TokenType.WHILE)
                            {
                                SkipBlock(tokens);
                                _currentPos--;
                                _blocks.Pop();
                            }
                            else if (!statementResult && !isElseStatement)
                            {
                                SkipBlock(tokens);
                                _currentPos--;
                            }
                            else if(isElseStatement && _blocks.Peek().Result)
                            {
                                SkipBlock(tokens);
                                _blocks.Pop();
                                _currentPos--;
                                isElseStatement = false;
                            }
                        }

                        break;
                    case TokenType.RIGHT_BRACE:
                        rightBraceIndex = _currentPos;

                        if(_blocks.Count() != 0)
                        {
                            if (isElseStatement)
                            {
                                _blocks.Pop();
                                isElseStatement = false;
                            }
                            else
                            {
                                if (_blocks.Peek().Block == TokenType.WHILE && _blocks.Peek().Result)
                                {
                                    _currentPos = _blocks.Peek().BodyIndex;
                                }
                                else if (_blocks.Peek().Block == TokenType.WHILE && !_blocks.Peek().Result)
                                    _blocks.Pop();
                                else if (_blocks.Peek().Block == TokenType.IF)
                                {
                                    if (_blocks.Peek().Result)
                                    {
                                        if (nextToken.Type == TokenType.ELSE_IF || nextToken.Type == TokenType.ELSE)
                                        {
                                            _currentPos = tokens.FindIndex(_currentPos, x => x.Type == TokenType.LEFT_BRACE);

                                            SkipBlock(tokens);

                                            if (tokens[_currentPos].Type != TokenType.ELSE_IF && tokens[_currentPos].Type != TokenType.ELSE)
                                                _blocks.Pop();

                                            _currentPos--;
                                        }
                                        else if (!isElseStatement)
                                        {
                                            _blocks.Pop();
                                        }
                                    }

                                    isIfStatement = false;
                                }
                            }
                        }

                        break;
                    case TokenType.INCREMENT:
                        if (prevToken.Type != TokenType.IDENT)
                            throw new Exception("Bad syntax!");

                        if (_variables.FirstOrDefault(x => x.Name == prevToken.Data) == null)
                            throw new Exception("Bad syntax!");
                        else
                        {
                            var variable = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            if (variable.Type == "number")
                            {
                                double value = Double.Parse(variable.Value.Replace('.', ','));
                                value++;
                                variable.Value = value.ToString();
                            }
                        }
                        break;
                    case TokenType.DECREMENT:
                        if (prevToken.Type != TokenType.IDENT)
                            throw new Exception("Bad syntax!");

                        if (_variables.FirstOrDefault(x => x.Name == prevToken.Data) == null)
                            throw new Exception("Bad syntax!");
                        else
                        {
                            var variable = _variables.FirstOrDefault(x => x.Name == prevToken.Data);
                            if (variable.Type == "number")
                            {
                                double value = Double.Parse(variable.Value.Replace('.', ','));
                                value++;
                                variable.Value = value.ToString();
                            }
                        }
                        break;
                    case TokenType.OPEN:
                        _arguments.Clear();
                        isFunction = true;
                        isOpenFunction = true;
                        break;
                    case TokenType.SUBMIT:
                        _arguments.Clear();
                        isFunction = true;
                        isSubmitFunction = true;
                        break;
                    case TokenType.FILL_TEXT:
                        _arguments.Clear();
                        isFunction = true;
                        isFillTextFuction = true;
                        break;
                    case TokenType.PRINT_TEXT:
                        _arguments.Clear();
                        isFunction = true;
                        isPrintFunction = true;
                        break;
                    case TokenType.CLICK:
                        _arguments.Clear();
                        isFunction = true;
                        isClickFunction = true;
                        break;
                    case TokenType.BACK:
                        _arguments.Clear();
                        isFunction = true;
                        isBackFunction = true;
                        break;
                    case TokenType.REFRESH:
                        _arguments.Clear();
                        isFunction = true;
                        isRefreshFunction = true;
                        break;
                    case TokenType.FORWARD:
                        _arguments.Clear();
                        isFunction = true;
                        isForwardFunction = true;
                        break;
                }
            }
        }

        private void SkipBlock(List<Token> tokens)
        {            
            do
            {
                _currentPos++;
                if (_tokens[_currentPos].Type == TokenType.LEFT_BRACE)
                {
                    SkipBlock(tokens);
                }
            }
            while (_tokens[_currentPos].Type != TokenType.RIGHT_BRACE);
            _currentPos++;
        }

        private bool Comparise(double left, double right, TokenType type)
        {
            if (type != TokenType.EQUALS && type != TokenType.LESS && type != TokenType.LESS_OR_EQUAL &&
                type != TokenType.NOT_EQUALS && type != TokenType.GREATER && type != TokenType.GREATER_OR_EQUAL)
                throw new ArgumentException("Bad arguments!");
            else
            {
                bool res;
                switch(type)
                {
                    case TokenType.EQUALS:
                        res = left == right;
                        if (res && _blocks.Peek().Block == TokenType.IF)
                            _blocks.Peek().Result = res;
                        else if (_blocks.Peek().Block == TokenType.WHILE)
                            _blocks.Peek().Result = res;

                        return res;
                    case TokenType.LESS:
                        res = left < right;
                        if (res && _blocks.Peek().Block == TokenType.IF)
                            _blocks.Peek().Result = res;
                        else if (_blocks.Peek().Block == TokenType.WHILE)
                            _blocks.Peek().Result = res;

                        return res;
                    case TokenType.LESS_OR_EQUAL:
                        res = left <= right;
                        if (res && _blocks.Peek().Block == TokenType.IF)
                            _blocks.Peek().Result = res;
                        else if (_blocks.Peek().Block == TokenType.WHILE)
                            _blocks.Peek().Result = res;

                        return res;
                    case TokenType.NOT_EQUALS:
                        res = left != right;
                        if (res && _blocks.Peek().Block == TokenType.IF)
                            _blocks.Peek().Result = res;
                        else if (_blocks.Peek().Block == TokenType.WHILE)
                            _blocks.Peek().Result = res;

                        return res;
                    case TokenType.GREATER:
                        res = left > right;
                        if (res && _blocks.Peek().Block == TokenType.IF)
                            _blocks.Peek().Result = res;
                        else if (_blocks.Peek().Block == TokenType.WHILE)
                            _blocks.Peek().Result = res;

                        return res;
                    case TokenType.GREATER_OR_EQUAL:
                        res = left >= right;
                        if (res && _blocks.Peek().Block == TokenType.IF)
                            _blocks.Peek().Result = res;
                        else if (_blocks.Peek().Block == TokenType.WHILE)
                            _blocks.Peek().Result = res;

                        return res;
                    default:
                        return false;
                }
            }
        }

        private void Submit(string id)
        {
            var element = Driver.FindElementById(id);
            element.Submit();
        }

        private void FillText(string id, string text)
        {
            var element = Driver.FindElementById(id);
            element.SendKeys(text);
        }
    }
}

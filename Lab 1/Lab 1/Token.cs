﻿namespace Lab_1
{
    public sealed class Token
    {
        public string Data { get; private set; }
        public TokenType Type { get; private set; }

        public Token() { }
        public Token(string data, TokenType type)
        {
            Data = data;
            Type = type;
        }

        public override string ToString()
        {
            return "Data: " + Data + " Type: " + Type.ToString();
        }
    }
}
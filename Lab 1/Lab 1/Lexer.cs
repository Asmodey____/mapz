﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Lab_1
{
    public enum TokenType
    {
        IDENT,
        SEMICOLON,  // ;
        COMMA,      // ,
        EOF,
        LEFT_BRACKET,  // (
        RIGHT_BRACKET, // )
        LEFT_BRACE,  // {
        RIGHT_BRACE, // }

        // KEY_WORDS
        VAR_KEYWORD,

        // LITERALS
        STRING_LIT,
        NUMBER_LIT,

        // OPERATIONS
        ASSIGN_OP,   // =

        // OPERATORS
        EQUALS,         // ==
        NOT_EQUALS,     // !=
        GREATER,      // >
        LESS,      // <
        GREATER_OR_EQUAL,  // >=
        LESS_OR_EQUAL,  // <=
        INCREMENT,      // ++
        DECREMENT,      // --

        // FUNCTIONS
        OPEN,
        SUBMIT,
        CLICK,
        FILL_TEXT,
        PRINT_TEXT,
        REFRESH,
        BACK,
        FORWARD,

        // BLOCKS
        WHILE,
        IF,
        ELSE_IF,
        ELSE,

        // IGNORABLE
        WHITE_SPACE,
        NEW_LINE,
        COMMENT,
        UNDEFINED,

        //Main
        BODY
    }

    public class Lexer
    {

        private string _inputCode;
        private int _index;
        private readonly Dictionary<TokenType, string> _tokens;
        private readonly Dictionary<TokenType, MatchCollection> _regExMatchCollection;

        public Lexer(string input)
        {
            _inputCode = input;
            _index = 0;
            _tokens = new Dictionary<TokenType, string>();
            _regExMatchCollection = new Dictionary<TokenType, MatchCollection>();

            // KEY_WORDS
            _tokens.Add(TokenType.VAR_KEYWORD, @"\bvar\b");

            // LITERALS
            _tokens.Add(TokenType.NUMBER_LIT, @"-?\d+(?:\,\d+)?");
            _tokens.Add(TokenType.STRING_LIT, "\".*?\"");

            // OPERATORS
            _tokens.Add(TokenType.DECREMENT, "\\-\\-");
            _tokens.Add(TokenType.EQUALS, "\\=\\=");
            _tokens.Add(TokenType.INCREMENT, "\\+\\+");
            _tokens.Add(TokenType.LESS_OR_EQUAL, "\\<\\=");
            _tokens.Add(TokenType.LESS, "\\<");
            _tokens.Add(TokenType.GREATER_OR_EQUAL, "\\>\\=");
            _tokens.Add(TokenType.GREATER, "\\>");
            _tokens.Add(TokenType.NOT_EQUALS, "\\!\\=");

            // OPERATIONS
            _tokens.Add(TokenType.ASSIGN_OP, "\\=");

            // EMBEDDED_FUNCTIONS
            _tokens.Add(TokenType.OPEN, "Open");
            _tokens.Add(TokenType.SUBMIT, "Submit");
            _tokens.Add(TokenType.CLICK, "Click");
            _tokens.Add(TokenType.FILL_TEXT, "FillText");
            _tokens.Add(TokenType.PRINT_TEXT, "PrintText");
            _tokens.Add(TokenType.REFRESH, "Refresh");
            _tokens.Add(TokenType.BACK, "Back");
            _tokens.Add(TokenType.FORWARD, "Forward");

            // BLOCKS
            _tokens.Add(TokenType.WHILE, @"\bwhile\b");
            _tokens.Add(TokenType.IF, @"\bif\b");
            _tokens.Add(TokenType.ELSE_IF, @"\belse_if\b");
            _tokens.Add(TokenType.ELSE, @"\belse\b");

            // IGNORABLE
            _tokens.Add(TokenType.NEW_LINE, "\\n");
            _tokens.Add(TokenType.COMMENT, "\\//");
            _tokens.Add(TokenType.WHITE_SPACE, "[ \\t]+");

            // DEFAULTS
            _tokens.Add(TokenType.COMMA, "\\,");
            _tokens.Add(TokenType.LEFT_BRACKET, "\\(");
            _tokens.Add(TokenType.RIGHT_BRACKET, "\\)");
            _tokens.Add(TokenType.LEFT_BRACE, "\\{");
            _tokens.Add(TokenType.RIGHT_BRACE, "\\}");
            _tokens.Add(TokenType.IDENT, @"\b(?!var |object |ClickButton |ClickLink |FillTextField |Print |for |while |if |else_if |else )\b[a-zA-Z_][a-zA-Z0-9_]*");
            _tokens.Add(TokenType.SEMICOLON, "\\;");

            PrepareRegex();
        }

        private void PrepareRegex()
        {
            _regExMatchCollection.Clear();
            foreach (KeyValuePair<TokenType, string> pair in _tokens)
            {
                _regExMatchCollection.Add(pair.Key, Regex.Matches(_inputCode, pair.Value));
            }
        }

        public void ResetParser()
        {
            _index = 0;
            _inputCode = string.Empty;
            _regExMatchCollection.Clear();
        }

        public Token GetToken()
        {
            if (_index >= _inputCode.Length)
            {
                return new Token(string.Empty, TokenType.EOF);
            }

            foreach (KeyValuePair<TokenType, MatchCollection> pair in _regExMatchCollection)
            {
                foreach (Match match in pair.Value)
                {
                    if (match.Index == _index)
                    {
                        _index += match.Length;
                        return new Token(match.Value, pair.Key);
                    }

                    if (match.Index > _index)
                    {
                        break;
                    }
                }
            }
            _index++;
            return new Token(string.Empty, TokenType.UNDEFINED);
        }
    }
}

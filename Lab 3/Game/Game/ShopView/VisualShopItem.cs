﻿using Models.ShopClasses;
using Models.Weapons;

namespace Game.ShopView
{
    public sealed class VisualShopItem
    {
        public string ImagePath { get; private set; }

        public ShopItem Item { get; private set; }

        public VisualShopItem(string imagePath, ShopItem item)
        {
            ImagePath = imagePath;
            Item = item;
        }
    }
}

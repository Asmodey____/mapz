﻿using Models.ShopClasses;
using Models.Weapons;

namespace Models
{
    //singleton
    public sealed class House
    {
        private int _hitPoints;

        private int _armor;

        private static House _house;

        public Shooter GetShooter { get; private set; }

        private static object _syncRoot = new object();

        //temp data
        private House()
        {
            _hitPoints = 1000;
            _armor = 0;

            GetShooter = new Shooter(Shop.GetShopInstance().Weapons[0].GetItem as Weapon);
        }

        public static House GetHouse()
        {
            if(_house == null)
            {
                lock (_syncRoot)
                {
                    if (_house == null)
                        _house = new House();
                }
            }
            
            return _house;
        }
    }
}

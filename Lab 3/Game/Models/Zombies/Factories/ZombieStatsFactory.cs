﻿using Models.Zombies.Stats.Armors;
using Models.Zombies.Stats.AttackTypes;
using Models.Zombies.Stats.Evasions;

namespace Models.Zombies.Factories
{
    public enum FactoriesConstants : uint
    {
        Classic,
        Tank,
        Runner
    }

    public abstract class ZombieStatsFactory
    {
        protected int _unchangebleFactor;

        protected int _factor = 1;

        public abstract Evasion CreateEvasion();

        public abstract Armor CreateArmor();

        public abstract AttackType CreateAttackType();

        public abstract int CreateKillReward();

        public abstract int CreateHp();

        public abstract string CreateName();

        public virtual void ChangeFactor(int factor)
        {
            _factor = factor;
        }
    }
}

﻿using System;

namespace Models.Zombies.Strategies
{
    public sealed class NormalZombieSpawner : ZombieSpawner
    {
        public override void SpawnZombies(int wave)
        {
            for (int i = 0, count = (int)Math.Round(wave * 2.5); i < count; i++)
                _zombies.Add(CreateClassicZombie(wave));

            if (wave > 3)
            {
                for (int i = 0, count = (int)Math.Round(wave * 1.8); i < count; i++)
                    _zombies.Add(CreateTankZombie(wave));
            }

            if (wave > 5)
            {
                for (int i = 0, count = (int)Math.Round(wave * 1.4); i < count; i++)
                    _zombies.Add(CreateRunnerZombie(wave));
            }
        }
    }
}

﻿namespace Models.Zombies.Stats.Evasions
{
    public abstract class Evasion
    {
        public int Factor { get; set; }

        public Evasion(int factor)
        {
            Factor = factor;
        }

        public override string ToString()
        {
            return "Evasion";
        }
    }
}

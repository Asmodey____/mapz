﻿namespace Models.Zombies.Stats.Evasions
{
    public sealed class CommonEvasion : Evasion
    {
        public CommonEvasion(int factor) : base(factor)
        {
        }

        public override string ToString()
        {
            return "Common Evasion";
        }
    }
}

﻿using Models.Zombies.Stats.Evasions;

namespace Models.Zombies.Stats.Armors.ArmorDecorators
{
    public sealed class EvasionArmor : ArmorDecorator
    {
        public Evasion GetAdditionalEvasion { get; }

        public EvasionArmor(Evasion evasion, Armor armor) : base(armor.Factor)
        {
            _armor = armor;
            GetAdditionalEvasion = evasion;
        }

        public override string ToString()
        {
            return "Evasion " + base.ToString();
        }
    }
}

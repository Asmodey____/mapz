﻿using System;

namespace Models.Weapons.SniperRifles
{
    [Serializable]
    public sealed class M24 : SniperRifle, ICloneable
    {
        public M24() : base("M24", 150, 1, 15)
        {
        }

        public override SniperRifle Clone()
        {
            return new M24();
        }

        object ICloneable.Clone()
        {
            return new M24();
        }
    }
}

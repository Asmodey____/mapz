﻿using System;

namespace Models.Weapons.Rifles
{
    [Serializable]
    public sealed class M16 : Rifle, ICloneable
    {
        public M16() : base("M16", 55, 90, 30)
        {
        }

        public override Rifle Clone()
        {
            return new M16();
        }

        object ICloneable.Clone()
        {
            return new M16();
        }
    }
}

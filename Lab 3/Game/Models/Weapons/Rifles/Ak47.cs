﻿using System;

namespace Models.Weapons.Rifles
{
    [Serializable]
    public sealed class Ak47 : Rifle, ICloneable
    {
        public Ak47() : base("Ak47", 70, 70, 30)
        {
        }

        public override Rifle Clone()
        {
            return new Ak47();
        }

        object ICloneable.Clone()
        {
            return new Ak47();
        }
    }
}

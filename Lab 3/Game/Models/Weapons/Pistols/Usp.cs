﻿using System;

namespace Models.Weapons.Pistols
{
    [Serializable]
    public sealed class Usp : Pistol, ICloneable
    {
        public Usp() : base("Usp", 40, 40, 20)
        {
            MAX_BULLETS = 20;
        }

        public override Pistol Clone()
        {
            return new Usp();
        }

        object ICloneable.Clone()
        {
            return new Usp();
        }
    }
}

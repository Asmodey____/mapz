﻿using System;

namespace Models.Weapons.Pistols
{
    [Serializable]
    public sealed class Glock : Pistol, ICloneable
    {
        public Glock() : base("Glock", 25, 50, 30)
        {
            MAX_BULLETS = 30;
        }

        public override Pistol Clone()
        {
            return new Glock();
        }

        object ICloneable.Clone()
        {
            return new Glock();
        }
    }
}

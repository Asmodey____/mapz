﻿using System;

namespace Models.Weapons.SniperRifles
{
    [Serializable]
    public sealed class Mini14 : SniperRifle, ICloneable
    {
        public Mini14() : base("Sls", 100, 30, 30)
        {
        }

        public override SniperRifle Clone()
        {
            return new Mini14();
        }

        object ICloneable.Clone()
        {
            return new Mini14();
        }
    }
}

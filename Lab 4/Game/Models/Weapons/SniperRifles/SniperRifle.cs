﻿using System;

namespace Models.Weapons.SniperRifles
{
    [Serializable]
    public abstract class SniperRifle : Weapon
    {
        public SniperRifle(string name, int damage, int fireRate, int bulletCount) : base(name, damage, fireRate, bulletCount)
        {
        }

        public abstract SniperRifle Clone();
    }
}

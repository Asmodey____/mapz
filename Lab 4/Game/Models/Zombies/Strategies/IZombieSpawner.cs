﻿namespace Models.Zombies.Strategies
{
    public interface IZombieSpawner
    {
        void SpawnZombies(int wave);

        Zombie GetZombie();

        int RemoveKilledZombie(Zombie zombie);
    }
}

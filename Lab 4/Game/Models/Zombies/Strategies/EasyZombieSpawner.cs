﻿namespace Models.Zombies.Strategies
{
    public sealed class EasyZombieSpawner : ZombieSpawner
    {
        public override void SpawnZombies(int wave)
        {
            base.SpawnZombies(wave);
        }
    }
}

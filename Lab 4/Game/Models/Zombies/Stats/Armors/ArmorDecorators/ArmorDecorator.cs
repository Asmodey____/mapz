﻿namespace Models.Zombies.Stats.Armors.ArmorDecorators
{
    public abstract class ArmorDecorator : Armor
    {
        protected Armor _armor;

        public ArmorDecorator(int factor) : base(factor)
        {
        }
    }
}

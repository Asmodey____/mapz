﻿using System;
using System.Runtime.Serialization;

namespace Models.ShopClasses.ShopExceptions
{
    public sealed class ItemSoldOutException : Exception
    {
        public ItemSoldOutException()
        {
        }

        public ItemSoldOutException(string message) : base(message)
        {
        }

        public ItemSoldOutException(string message, Exception innerException) : base(message, innerException)
        {
        }

        private ItemSoldOutException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

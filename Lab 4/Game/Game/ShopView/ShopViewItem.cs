﻿namespace Game.ShopView
{
    class ShopViewItem
    {
        public VisualShopItem Item1 { get; set;}

        public VisualShopItem Item2 { get; set; }

        public VisualShopItem Item3 { get; set; }

        public ShopViewItem(VisualShopItem item1, VisualShopItem item2, VisualShopItem item3)
        {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
        }
    }
}

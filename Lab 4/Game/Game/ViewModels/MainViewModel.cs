﻿using Game.Commands;
using Models;
using Models.SavingClasses;
using Models.ShopClasses;
using Models.Weapons;
using Models.Zombies;
using Models.Zombies.Strategies;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Game.ViewModels
{
    //facade
    public sealed class MainViewModel : BaseViewModel
    {
        private List<Weapon> _availableWeapon = House.GetHouse().GetShooter.AvailableWeapons;

        public List<Weapon> AvailableWeapon
        {
            get
            {
                return _availableWeapon;
            }
            private set
            {
                _availableWeapon = value;
                OnPropertyChanged(nameof(AvailableWeapon));
            }
        }

        private Weapon _selectedWeapon;

        public Weapon SelectedWeapon
        {
            get
            {
                return _selectedWeapon;
            }
            set
            {
                _selectedWeapon = value;
                House.GetHouse().GetShooter.CurrentWeapon = _selectedWeapon;
                OnPropertyChanged(nameof(SelectedWeapon));
            }
        }

        public bool IsLoadingLastSave { get; set; }

        private Difficulty _difficulty;

        public Difficulty GetDifficulty
        {
            get
            {
                return _difficulty;
            }
            set
            {
                _difficulty = value;
                _account.GetDifficulty = _difficulty;
            }
        }

        private bool _isNotWave = true;

        public bool IsNotWave
        {
            get { return _isNotWave; }
            set { _isNotWave = value; OnPropertyChanged(nameof(IsNotWave)); }
        }

        private Account _account = new Account("Lol");

        private IZombieSpawner _spawner;

        private int _maxHp;

        public int MaximumHp
        {
            get { return _maxHp; }
            set { _maxHp = value; OnPropertyChanged(nameof(MaximumHp)); }
        }

        private int _currentHp;

        public int CurrentHp
        {
            get { return _currentHp; }
            set { _currentHp = value; OnPropertyChanged(nameof(CurrentHp)); }
        }

        private string _progressInfo;

        public string ProgressInfo
        {
            get { return _progressInfo; }
            set { _progressInfo = value; OnPropertyChanged(nameof(ProgressInfo)); }
        }

        private string _timer;

        public string Timer
        {
            get { return _timer; }
            set { _timer = value; OnPropertyChanged(nameof(Timer)); }
        }

        public MainViewModel()
        {
            DifficultyWindow window = new DifficultyWindow(this);
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.ShowDialog();

            if(IsLoadingLastSave)
            {
                AccountSaver result = AccountSaver.LoadLast();

                if (result == null)
                {
                    MessageBox.Show("Error with loading save, default properties were setted", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);

                    _account.SetDefault();
                    House.GetHouse().GetShooter.SetDefault();
                }
                else
                {
                    _account.SetState(result);
                    House.GetHouse().GetShooter.SetState(result);
                }

                GetDifficulty = _account.GetDifficulty;
            }

            SetSpawner();

            AvailableWeapon = _account.GetHouse().GetShooter.AvailableWeapons;
            SelectedWeapon = _account.GetHouse().GetShooter.CurrentWeapon;

        }

        private void SetSpawner()
        {
            if (GetDifficulty == Difficulty.Easy)
                _spawner = new EasyZombieSpawner();
            else if (GetDifficulty == Difficulty.Normal)
                _spawner = new NormalZombieSpawner();
            else if (GetDifficulty == Difficulty.Hard)
                _spawner = new HardZombieSpawner();
            else
                throw new Exception("Data Context exception");
        }

        private RelayCommand _openShop;

        public RelayCommand OpenShop
        {
            get
            {
                return _openShop ??
                    (_openShop = new RelayCommand(obj =>
                    {
                        int money = _account.Money;
                        ShopWindow window = new ShopWindow(ref money, _availableWeapon);
                        window.ShowDialog();

                        var comboBox = obj as ComboBox;

                        if(comboBox == null)
                            throw new ArgumentException("Bad argument!");

                        comboBox.Items.Refresh();
                    }));
            }
        }


        private RelayCommand _clickOnZombie;

        public RelayCommand ClickOnZombie
        {
            get
            {
                return _clickOnZombie ??
                    (_clickOnZombie = new RelayCommand(obj =>
                    {
                        var textBlock = obj as TextBlock;
                        Zombie zombie;

                        if (textBlock == null)
                            throw new ArgumentException("Bad argument!");

                        if ( (zombie = _spawner.GetZombie()) != null)
                        {
                            StartTimer();
                            ProgressInfo = zombie.ToString();
                            MaximumHp = zombie.FullHp;
                            int result = House.GetHouse().GetShooter.Shoot(zombie);

                            if (result == -1)
                                textBlock.Text += "Miss" + "\n";
                            else
                            {
                                CurrentHp = result;
                                textBlock.Text += result + "\n";
                            }
                                

                            if (result == 0)
                            {
                                textBlock.Text += "Reward: " + zombie.KillReward + "\n";
                                _account.GetRewardForKill(zombie.KillReward);

                                if (_spawner.RemoveKilledZombie(zombie) == 0)
                                {
                                    IsNotWave = true;
                                    _account.Wave++;
                                }
                                else
                                {
                                    SetProgressBar(_spawner.GetZombie());
                                }
                            }
                        }
                    }));
            }
        }


        private RelayCommand _startWave;

        public RelayCommand StartWave
        {
            get
            {
                return _startWave ??
                    (_startWave = new RelayCommand(obj =>
                    {
                        var textBlock = obj as TextBlock;
                        if (textBlock == null)
                            throw new ArgumentException("Bad argument!");

                        textBlock.Text += "Wave [" + _account.Wave + "]\n";
                        _spawner.SpawnZombies(_account.Wave);
                        IsNotWave = false;

                        SetProgressBar(_spawner.GetZombie());
                        
                    }));
            }
        }

        private RelayCommand _selectWeaponCommand;

        public RelayCommand SelectWeaponCommand
        {
            get
            {
                return _selectWeaponCommand ??
                    (_selectWeaponCommand = new RelayCommand(obj =>
                    {
                        
                    }));
            }
        }

        private RelayCommand _saveCommand;

        public RelayCommand SaveCommand
        {
            get
            {
                return _saveCommand ??
                    (_saveCommand = new RelayCommand(obj =>
                    {
                        var saver = new AccountSaver();
                        saver.Save(_account.Username, _account.Experience, _account.Level, _account.Wave, _account.Money,
                            House.GetHouse().GetShooter.AvailableWeapons, House.GetHouse().GetShooter.CurrentWeapon, _difficulty);
                    }));
            }
        }


        private void SetProgressBar(Zombie zombie)
        {
            MaximumHp = zombie.FullHp;
            CurrentHp = MaximumHp;
        }

        private void StartTimer()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += TickTimer;
            timer.Start();
        }

        private void TickTimer(object sender, EventArgs e)
        {
        }
    }
}

﻿using Game.Commands;
using Models;
using Models.ShopClasses;
using Models.ShopClasses.ShopExceptions;
using Models.Weapons;
using Models.Weapons.Pistols;
using Models.Weapons.Rifles;
using Models.Weapons.SniperRifles;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Game.ViewModels
{
    public sealed class ShopViewModel : BaseViewModel
    {

        private Shop _shop { get; } = Shop.GetShopInstance();

        private int _money;

        private List<Weapon> _availableWeapon;

        private string _moneyLabelText;

        public string MoneyLabelText
        {
            get
            {
                return _moneyLabelText;
            }
            private set
            {
                _moneyLabelText = value;
                OnPropertyChanged(nameof(MoneyLabelText));
            }
        }

        //add experience in the future
        public ShopViewModel(Shop shop, ref int money, List<Weapon> availableWeapon)
        {
            _shop = shop;
            _money = money;
            _availableWeapon = availableWeapon;
            MoneyLabelText = "Money: " + money;
        }
        

        private RelayCommand _backCommand;

        public RelayCommand BackCommand
        {
            get
            {
                return _backCommand ??
                    (_backCommand = new RelayCommand(obj =>
                    {
                        var window = obj as Window;

                        if (window == null)
                            throw new ArgumentException("Bad argument!");

                        window.Close();
                    }));
            }
        }

        private RelayCommand _buyCommand;

        public RelayCommand BuyCommand
        {
            get
            {
                return _buyCommand ??
                    (_buyCommand = new RelayCommand(obj =>
                    {
                        var item = obj as ListBoxItem;

                        if (item == null)
                            throw new ArgumentException("Bad argument!");

                        var weapon = item.Content as ShopItem;
                        if (weapon == null)
                            throw new Exception("Something went wrong!");

                        try
                        {
                            _money = _shop.BuyWeapon(weapon, _money, _availableWeapon);
                            MoneyLabelText = "Money: " + _money;
                        }
                        catch(ItemSoldOutException ex)
                        {
                            MessageBox.Show("Item is already sold out!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        catch(NotEnoughMoneyException ex)
                        {
                            MessageBox.Show("Not enough money!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                        if (weapon.GetItem as Weapon == null)
                            throw new ArgumentException("Bad argument!");
                       
                    }));
            }
        }
    }
}

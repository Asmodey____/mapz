﻿using System;
using System.Collections.Generic;
using Models;
using Models.SavingClasses;
using Models.Zombies.Strategies;

namespace Game
{
    public sealed class Account
    {
        public string Username { get; private set; }

        public int Experience { get; set; } = 1; // temp setter

        public int Level { get; set; } = 1; // temp setter

        public int Wave { get; set; } // temp setter

        public int Money { get; private set; } = 100; // temp value

        public Difficulty GetDifficulty { get; set; } = Difficulty.Easy;

        private House _house = House.GetHouse();

        public House GetHouse() => _house;

        public Account(string username)
        {
            Username = username;
            Wave = 1;
        }

        public int GetRewardForKill(int reward)
        {
            Money += reward;
            return Money;
        }

        public void SetDefault()
        {
            Username = "";
            Experience = 1;
            Level = 1;
            Wave = 1;
            Money = 100;
            GetDifficulty = Difficulty.Easy;
        }

        public void SetState(AccountSaver saver)
        {
            Username = saver.Username;
            Experience = saver.Experience;
            Level = saver.Level;
            Wave = saver.Wave;
            Money = saver.Money;
            GetDifficulty = saver.GetDifficulty;
        }

        //public AccountSaver SaveState()
        //{
        //    var saver = new AccountSaver();
        //    saver.Save(Username, Experience, Level, Wave, Money, GetDifficulty);
        //    return saver;
        //}

        //public bool Load()
        //{
        //    var saver = AccountSaver.LoadLast();

        //    if (saver == null)
        //    {
        //        Username = "";
        //        Experience = 1;
        //        Level = 1;
        //        Wave = 1;
        //        Money = 100;
        //        GetDifficulty = Difficulty.Easy;

        //        return false;
        //    }


        //    Username = saver.Username;
        //    Experience = saver.Experience;
        //    Level = saver.Level;
        //    Wave = saver.Wave;
        //    Money = saver.Money;
        //    GetDifficulty = saver.GetDifficulty;

        //    return true;
        //}
    }
}

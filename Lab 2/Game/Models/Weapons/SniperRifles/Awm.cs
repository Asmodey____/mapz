﻿using System;

namespace Models.Weapons.SniperRifles
{
    [Serializable]
    public sealed class Awm : SniperRifle, ICloneable
    {
        public Awm() : base("Awm", 200, 1, 10)
        {

        }

        public override SniperRifle Clone()
        {
            return new Awm();
        }

        object ICloneable.Clone()
        {
            return new Awm();
        }
    }
}

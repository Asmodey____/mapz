﻿using System;

namespace Models.Weapons.Pistols
{
    [Serializable]
    public sealed class Deagle : Pistol, ICloneable
    {
        public Deagle() : base("Deagle", 70, 30, 7)
        {

        }

        public override Pistol Clone()
        {
            return new Deagle();
        }

        object ICloneable.Clone()
        {
            return new Deagle();
        }
    }
}

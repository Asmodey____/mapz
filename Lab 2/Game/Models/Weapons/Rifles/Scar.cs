﻿using System;

namespace Models.Weapons.Rifles
{
    [Serializable]
    public sealed class Scar : Rifle, ICloneable
    {
        public Scar() : base("Scar", 70, 85, 30)
        {
        }

        public override Rifle Clone()
        {
            return new Scar();
        }

        object ICloneable.Clone()
        {
            return new Scar();
        }
    }
}

﻿using Models.Weapons;
using Models.Zombies.Strategies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Models.SavingClasses
{
    [Serializable]
    public sealed class AccountSaver
    {
        public string Username { get; private set; }

        public int Experience { get; private set; }

        public int Level { get; private set; }

        public int Wave { get; private set; }

        public int Money { get; private set; }

        public string Date { get; private set; }

        public List<Weapon> AvailableWeapons { get; private set; }

        public Weapon CurrentWeapon { get; private set; }

        public Difficulty GetDifficulty { get; private set; }

        public void Save(string username, int exp, int lvl, int wave, int money, List<Weapon> weapons, 
            Weapon currentWeapon, Difficulty difficulty)
        {
            Username = username;
            Experience = exp;
            Level = lvl;
            Wave = wave;
            Money = money;
            GetDifficulty = difficulty;
            AvailableWeapons = weapons;
            CurrentWeapon = currentWeapon;

            BinaryFormatter formatter = new BinaryFormatter();

            using (FileStream file = new FileStream("LastSave.bin", FileMode.OpenOrCreate))
            {
                formatter.Serialize(file, this);
            }
        }

        public static AccountSaver LoadLast()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            AccountSaver saver = new AccountSaver();

            using (FileStream file = new FileStream("LastSave.bin", FileMode.OpenOrCreate))
            {
                try
                {
                    saver = (AccountSaver)formatter.Deserialize(file);
                }
                catch(ArgumentNullException ex)
                {
                    return null;
                }
            }

            return saver;
        }

        public override string ToString()
        {
            return Date;
        }
    }
}

﻿using Models.Zombies;
using Models.Zombies.Factories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Models.Zombies.Strategies
{
    public enum Difficulty : uint
    {
        Easy,
        Normal,
        Hard
    }

    //flyweight
    public abstract class ZombieSpawner : IZombieSpawner
    {
        protected List<Zombie> _zombies = new List<Zombie>();

        protected Dictionary<FactoriesConstants, ZombieStatsFactory> _factories = new Dictionary<FactoriesConstants, ZombieStatsFactory>();

        public ZombieSpawner()
        {
            _factories.Add(FactoriesConstants.Classic, new ClassicZombieStatsFactory());
            _factories.Add(FactoriesConstants.Runner, new RunnerZombieStatsFactory());
            _factories.Add(FactoriesConstants.Tank, new TankZombieStatsFactory());
        }


        protected Zombie CreateClassicZombie(int wave)
        {
           _factories[FactoriesConstants.Classic].ChangeFactor(wave);
            return new Zombie(_factories[FactoriesConstants.Classic] as ClassicZombieStatsFactory);
        }

        protected Zombie CreateTankZombie(int wave)
        {
            _factories[FactoriesConstants.Tank].ChangeFactor(wave);
            return new Zombie(_factories[FactoriesConstants.Tank] as TankZombieStatsFactory);
        }

        protected Zombie CreateRunnerZombie(int wave)
        {
            _factories[FactoriesConstants.Runner].ChangeFactor(wave);
            return new Zombie(_factories[FactoriesConstants.Runner] as RunnerZombieStatsFactory);
        }

        public virtual void SpawnZombies(int wave)
        {
            for (int i = 0, count = (int)Math.Round(wave * 1.5); i < count; i++)
                _zombies.Add(CreateClassicZombie(wave));

            if (wave > 3)
            {
                for (int i = 0, count = (int)Math.Round(wave * 1.2); i < count; i++)
                    _zombies.Add(CreateTankZombie(wave));
            }

            if (wave > 5)
            {
                for (int i = 0, count = (int)Math.Round(wave * 0.75); i < count; i++)
                    _zombies.Add(CreateRunnerZombie(wave));
            }
        }

        public Zombie GetZombie()
        {
            if (_zombies.Count() > 0)
                return _zombies[0];
            else
                return null;
        }

        //returns remaining zombies
        public int RemoveKilledZombie(Zombie zombie)
        {
            _zombies.Remove(zombie);
            return _zombies.Count();
        }
    }
}

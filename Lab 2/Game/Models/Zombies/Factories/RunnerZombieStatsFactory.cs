﻿using Models.Zombies.Stats.Armors;
using Models.Zombies.Stats.Armors.ArmorDecorators;
using Models.Zombies.Stats.AttackTypes;
using Models.Zombies.Stats.Evasions;
using System;

namespace Models.Zombies.Factories
{
    //Temporary implementation
    public sealed class RunnerZombieStatsFactory : ZombieStatsFactory
    {
        public RunnerZombieStatsFactory()
        {
            _unchangebleFactor = 15;
        }

        public override Armor CreateArmor()
        {
            Random rand = new Random();
            var armor = new LiteArmor(_unchangebleFactor + rand.Next(_factor, 10 + _factor));

            if (_factor % 10 == 0)
            {
                HighEvasion evasion = new HighEvasion(30);
                EvasionArmor boostedArmor = new EvasionArmor(evasion, armor);
                return boostedArmor;
            }

            if (_factor % 5 == 0)
            {
                HpArmor boostedArmor = new HpArmor(_unchangebleFactor + 200 * _factor, armor);
                return boostedArmor;
            }

            return armor;
        }

        public override AttackType CreateAttackType()
        {
            return new QuickAttackType();
        }

        public override Evasion CreateEvasion()
        {
            Random rand = new Random();
            return new HighEvasion(_unchangebleFactor + rand.Next(0, 10));
        }

        public override int CreateHp()
        {
            return 70;
        }

        public override int CreateKillReward()
        {
            return _unchangebleFactor + _factor * 20;
        }

        public override string CreateName()
        {
            return "Runner Zombie";
        }
    }
}

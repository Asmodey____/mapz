﻿using Models.Zombies.Stats.Armors;
using Models.Zombies.Stats.Armors.ArmorDecorators;
using Models.Zombies.Stats.AttackTypes;
using Models.Zombies.Stats.Evasions;
using System;

namespace Models.Zombies.Factories
{
    //Temporary implementation
    public sealed class TankZombieStatsFactory : ZombieStatsFactory
    {
        public TankZombieStatsFactory()
        {
            _unchangebleFactor = 10;
        }

        public override Armor CreateArmor()
        {
            Random rand = new Random();
            var armor = new HeavyArmor(_unchangebleFactor + rand.Next(_factor, 10 + _factor));

            if (_factor % 10 == 0)
            {
                LowEvasion evasion = new LowEvasion(5);
                EvasionArmor boostedArmor = new EvasionArmor(evasion, armor);
                return boostedArmor;
            }

            if (_factor % 5 == 0)
            {
                HpArmor boostedArmor = new HpArmor(_unchangebleFactor + 200 * _factor, armor);
                return boostedArmor;
            }

            return armor;
        }

        public override AttackType CreateAttackType()
        {
            return new SlowAttackType();
        }

        public override Evasion CreateEvasion()
        {
            Random rand = new Random();
            return new LowEvasion(_unchangebleFactor + rand.Next(0, 10));
        }

        public override int CreateHp()
        {
            return 200;
        }

        public override int CreateKillReward()
        {
            return _unchangebleFactor + _factor * 15;
        }

        public override string CreateName()
        {
            return "Tank Zombie";
        }
    }
}

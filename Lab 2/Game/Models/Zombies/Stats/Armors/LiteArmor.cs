﻿namespace Models.Zombies.Stats.Armors
{
    public sealed class LiteArmor : Armor
    {
        public LiteArmor(int factor) : base(factor)
        {
        }

        public override string ToString()
        {
            return "Lite Armor";
        }
    }
}

﻿namespace Models.Zombies.Stats.Armors
{
    public abstract class Armor
    {
        public int Factor { get; protected set; }

        public Armor(int factor)
        {
            Factor = factor;
        }

        public override string ToString()
        {
            return "Armor";
        }
    }
}

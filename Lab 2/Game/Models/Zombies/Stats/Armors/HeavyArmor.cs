﻿namespace Models.Zombies.Stats.Armors
{
    public sealed class HeavyArmor : Armor
    {
        public HeavyArmor(int factor) : base(factor)
        {
        }

        public override string ToString()
        {
            return "Heavy Armor";
        }
    }
}

﻿using Models.Zombies.Factories;
using Models.Zombies.Stats.Armors;
using Models.Zombies.Stats.Armors.ArmorDecorators;
using Models.Zombies.Stats.AttackTypes;
using Models.Zombies.Stats.Evasions;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Models.Zombies
{
    public sealed class Zombie
    {
        private readonly string _name;

        private readonly Armor ARMOR;

        private readonly Evasion EVASION;

        private readonly AttackType ATTACK_TYPE;

        public int FullHp { get; private set; }

        public int KillReward { get; }

        public int CurrentHp { get; private set; }

        public Zombie(ZombieStatsFactory factory)
        {
            FullHp = factory.CreateHp();
            CurrentHp = FullHp;
            KillReward = factory.CreateKillReward();
            _name = factory.CreateName();

            ARMOR = factory.CreateArmor();
            EVASION = factory.CreateEvasion();
            ATTACK_TYPE = factory.CreateAttackType();

            if(ARMOR is HpArmor)
            {
                CurrentHp += (ARMOR as HpArmor).GetAdditionalHp;
                FullHp += (ARMOR as HpArmor).GetAdditionalHp;
            }
            else if(ARMOR is EvasionArmor)
            {
                EVASION.Factor += (ARMOR as EvasionArmor).GetAdditionalEvasion.Factor;
            }
        }

        public int GetDamage(int damage)
        {
            Random rand = new Random();
            var miss = rand.Next(0, 100);
            if (miss < EVASION.Factor)
                return -1;
            else
            {
                damage -= ARMOR.Factor;
                return (CurrentHp -= damage) > 0 ? CurrentHp : 0;
            }
        }

        public override string ToString()
        {
            return _name + /* + " Attack Type: " + ATTACK_TYPE*/
                ": " + ARMOR + ", " + EVASION + "\n";
        }
    }
}

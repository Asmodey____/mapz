﻿using System;

namespace Models
{
    [Serializable]
    public abstract class Item
    {
        public string Name { get; }

        public Item(string name)
        {
            Name = name;
        }
    }
}

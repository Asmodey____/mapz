﻿using Game.Commands;
using Models.SavingClasses;
using Models.Zombies.Strategies;
using System.Windows;
using System.Windows.Controls;

namespace Game.ViewModels
{
    public sealed class DifficultyViewModel : BaseViewModel
    {
        private bool IsOkEnabled { get; set; } = false;

        private MainViewModel _viewModel;

        public DifficultyViewModel(MainViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        private RelayCommand _chooseDifficultyCommand;

        public RelayCommand ChooseDifficultyCommand
        {
            get
            {
                return _chooseDifficultyCommand ??
                    (_chooseDifficultyCommand = new RelayCommand(obj =>
                    {
                        var button = obj as RadioButton;

                        if (button.Name == "ButtonEasy")
                            _viewModel.GetDifficulty = Difficulty.Easy;
                        else if (button.Name == "ButtonNormal")
                            _viewModel.GetDifficulty = Difficulty.Normal;
                        else if (button.Name == "ButtonHard")
                            _viewModel.GetDifficulty = Difficulty.Hard;

                        IsOkEnabled = true;
                    }));
            }
        }

        private RelayCommand _okCommand;

        public RelayCommand OkCommand
        {
            get
            {
                return _okCommand ??
                    (_okCommand = new RelayCommand(obj =>
                    {
                        var window = obj as Window;
                        window.Close();
                    }));
            }
        }

        private RelayCommand _loadCommand;

        public RelayCommand LoadCommand
        {
            get
            {
                return _loadCommand ??
                    (_loadCommand = new RelayCommand(obj =>
                    {
                        _viewModel.IsLoadingLastSave = true;
                        var window = obj as Window;
                        window.Close();
                    }));
            }
        }
    }
}
